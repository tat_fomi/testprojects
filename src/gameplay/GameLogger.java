package gameplay;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

public class GameLogger {
	 private static volatile GameLogger instance;
	 private File file;
	 
	 private GameLogger() throws IOException {
		 file = new File("C:\\Java\\workspace\\ConsoleHeroes\\src\\GameLog");
	 }
	   
	 public static GameLogger getInstance() throws InterruptedException, IOException {
	     if (instance == null) {
	    	 instance = new GameLogger();
	         }
	    return instance;
	}
	 
	 public void log(String comment) throws IOException {
		 FileWriter writer = new FileWriter (file);
		 writer.write(comment);
		 writer.append('\n');
		 writer.flush();
		 writer.close();
		 System.out.println(comment);
	 }
}

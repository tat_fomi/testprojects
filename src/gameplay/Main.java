package gameplay;

import java.io.IOException;
import java.util.Random;

import units.Group;
import units.Race;
import units.Unit;

public class Main {
	public static void main(String [] args) throws IOException, InterruptedException {
		 Group first = new Group(Race.ELF);
		 Group second = new Group(Race.ORC);
		 boolean isFirstGroup = Math.random()>0.5;
		 while (first.isSomebodyAlive() || second.isSomebodyAlive()) {
			 if (isFirstGroup) {
				 first.getAttacker().combat(second.getTarget());
				 second.getAttacker().combat(first.getTarget());
			 } else {
				 second.getAttacker().combat(first.getTarget());
				 first.getAttacker().combat(second.getTarget());
			 }
		 } 
		 if (first.isSomebodyAlive()) {
			 GameLogger.getInstance().log("First group is a winner");
		 } else {
			 GameLogger.getInstance().log("Second group is a winner");
		 }

	}
	
}

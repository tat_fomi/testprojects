package units;

public interface IRangeUnit {
	void longRangeAttack (Unit target);
	void meleeAttack (Unit target);
}

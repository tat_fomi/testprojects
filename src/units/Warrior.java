package units;

public class Warrior extends Unit {
	private String attackType = "sword";
	
	public Warrior() {
		super();
	}
	
	public Warrior(Race race) {
		super();
		this.race = race;
		switch (this.race) {
		case ELF:
			this.damage = 15; 
			break;
		case HUMAN:
			this.damage = 18;
			break;
		case ORC:
			this.damage = 20;
			this.attackType = "cudgel";
			break;
		case UNDEAD:
			this.damage = 5;
			this.attackType = "spear";
			break;
		}
	}
	
	@Override
	public void combat(Unit target) {
		double allDamage = damage*multiplier;
		target.hasBeenAttacked(allDamage, attackType, this);
	}

}

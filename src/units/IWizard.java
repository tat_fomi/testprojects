package units;

public interface IWizard {
	void improveUnit(Unit unit);
	void magicAttack(Unit target);
}

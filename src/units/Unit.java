package units;

import java.io.IOException;

import gameplay.GameLogger;

public abstract class Unit {
	double helthPoints;
	double damage;
	double multiplier = 1;
	boolean isImproved;
	Race race;
	boolean isAlive;
	
	Unit () {
		helthPoints = 100;
		isImproved = false;
		isAlive = true;
	}
	
	public abstract void combat(Unit target);
	
	public void becomeImproved(Unit wizard, boolean isNotUndead) {
		String comment = null;
		if (isNotUndead) {
			if (!isImproved && isAlive) {
				multiplier = multiplier * 1.5;
				isImproved = true;
				comment = String.format("The %s made %s improved.", 
						wizard, this);  
			} else {
				comment = String.format("The %s tried to improve %s. The %s is already improved", 
						wizard, this, this); 
			}
		} else {
			if (isAlive) {
				multiplier = multiplier * 0.5;
				comment = String.format("The %s made %s sick.", 
						wizard, this);  
			}
		}
		logComment(comment);
	}
	
	@Override
	public String toString() {
		return String.format(this.getRace().toString() + " " + this.getClass());
	}
	
	public void getCursed(Unit shaman) {
		if (isAlive && isImproved) {
			this.multiplier = multiplier / 1.5;
			logComment(String.format("%s put a curse on %s", shaman, this));
		} else {
			logComment(String.format("%s tried to put a curse on %s, who is not improved ", 
					shaman, this));
		}
	}
	
	public void logComment(String comment) {
		try {
			GameLogger.getInstance().log(comment);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void hasBeenAttacked (double damage, String attackType, Unit attacker) {
		String comment = null;
		if (this.getHelthPoints()-damage > 0) {
			this.setHelthPoints(this.getHelthPoints()-damage);
			comment = String.format("The %s was attacked by %s with %s. %d points of damage. %d HP left", 
					this, attacker, attackType, (long)damage, (long) this.getHelthPoints());  
		} else {
			comment = String.format("The %s was killed with %s by %s.", 
					this, attackType, attacker);  
			this.isAlive = false;
		}
		logComment(comment);
	}
	
	public double getHelthPoints() {
		return helthPoints;
	}

	public void setHelthPoints(double helthPoints) {
		this.helthPoints = helthPoints;
	}

	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
	}

	public boolean isImproved() {
		return isImproved;
	}

	public void setImproved(boolean isImproved) {
		this.isImproved = isImproved;
	}

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	}

}

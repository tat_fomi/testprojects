package units;

public class RangeUnit extends Unit implements IRangeUnit{
	private double rangeDamage;
	private String rangeAttackType = "bow";
	private String meleeAttackType;

	public RangeUnit() {
		super();
		rangeDamage = 0;
	}
	
	public RangeUnit(Race race) {
		super();
		this.race = race;
		switch (this.race) {
			case ELF:
				this.damage = 3;
				this.rangeDamage = 7;
				this.meleeAttackType = "melee";
				break;
			case HUMAN:
				this.damage = 3;
				this.rangeDamage = 5;
				this.rangeAttackType = "crossbow";
				this.meleeAttackType = "melee";
				break;
			case ORC:
				this.damage = 2;
				this.rangeDamage = 3;
				this.meleeAttackType = "blade";
				break;
			case UNDEAD:
				this.damage = 2;
				this.rangeDamage = 4;	
				this.meleeAttackType = "melee";
				break;
		}
	}
	
	@Override
	public void longRangeAttack(Unit target) {
		double allDamage = rangeDamage*multiplier;
		target.hasBeenAttacked(allDamage, rangeAttackType, this);
	}

	@Override
	public void meleeAttack(Unit target) {
		double allDamage = rangeDamage*multiplier;
		target.hasBeenAttacked(allDamage, meleeAttackType, this);
	}

	@Override
	public void combat(Unit target) {
		double randomNumber = Math.random();
		if (randomNumber>0.5) {
			longRangeAttack(target);
		} else {
			meleeAttack(target);
		}
	}

}

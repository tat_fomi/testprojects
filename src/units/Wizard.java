package units;

public class Wizard extends Unit implements IWizard{
	 
	public Wizard() {
		super();
	}
	
	public Wizard(Race race) {
		super();
		this.race = race;
		switch (this.race) {
		case ELF:
			this.damage = 10; 
			break;
		case HUMAN:
			this.damage = 4;
			break;
		case UNDEAD:
			this.damage = 5;
			break;
		} 
		
	}

	@Override
	public void improveUnit(Unit unit) {
		boolean isNotUndead = !this.getRace().equals(Race.UNDEAD);
		unit.becomeImproved(this, isNotUndead);
	}

	@Override
	public void magicAttack(Unit target) {
		double allDamage = damage*multiplier;
		target.hasBeenAttacked(allDamage, "magic", this);
	}

	@Override
	public void combat(Unit target) {
	if (target.getRace().equals(Race.ELF) || target.getRace().equals(Race.HUMAN)) {
			improveUnit(target);
	} else {
			magicAttack(target);
	}
	}
}

package units;

public class Shaman extends Unit {
		public Shaman() {
			super();
			
		}
		
		public void improveUnit(Unit unit) {
			unit.becomeImproved(this, true);
		}

		public void putCurse(Unit unit) {
			unit.getCursed(this);
		}
		
		@Override
		public void combat(Unit target) {
			if (target.getRace().equals(Race.ORC)|| target.getRace().equals(Race.UNDEAD)) {
				improveUnit(target);
			} else {
				putCurse(target);
			}
		}
		
		
}

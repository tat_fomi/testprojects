package units;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Group {
	private Race race;
	private List<Unit> units;
	
	public Group(Race race) {
		units = new ArrayList <Unit> (8);
		this.race = race;
		units.add(new Wizard(race));
		for (int i = 0; i < 3; i++) {
			units.add(new RangeUnit(race));
		}
		for (int i = 0; i < 4; i++) {
			units.add(new Warrior(race));
		}
	}
	
	public Unit getAttacker() {
		Random rnd = new Random();
		List<Unit> imprUnits = new ArrayList <Unit>();
		for (Unit u : units) {
			if (u.isImproved) {
				imprUnits.add(u);
			}
		}
		if (!imprUnits.isEmpty()) {
			return imprUnits.get(rnd.nextInt(imprUnits.size()));
		} else {
			return units.get(rnd.nextInt(units.size()));
		}
	}
	
	public Unit getTarget() {
		Random rnd = new Random();
		return units.get(rnd.nextInt(units.size()));
	}
	
	public boolean isSomebodyAlive() {
		for (Unit u : units) {
			if (u.isAlive) {
				return true;
			}
		}
		return false;
	}
}
